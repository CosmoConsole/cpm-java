package net.memmove.java.cpm;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public class BitInputStream extends InputStream {
	ByteArrayInputStream stream;
	int within = 8;
	int lastByte = 0;
	int pos = 0;
	public BitInputStream(byte[] data) {
		stream = new ByteArrayInputStream(data);
	}
	@Override
	public int read() throws IOException {
		return (int)this.readVariable(8);
	}
	public long readVariable(int bitSize) {
		long empty = 0;
		int bitsLeft = bitSize;
		while (bitsLeft > 0) {
			empty <<= 1;
			if (within >= 8) {
				lastByte = stream.read();
				pos++;
				if (lastByte < 0)
					return -1;
				within = 0;
			}
			int w = 7-within;
			empty |= (lastByte&(1<<w))>>w;
			within++;
			bitsLeft--;
		}
		return empty;
	}
	@Override
	public void close() throws IOException {
		stream.close();
	}
	public void pad() {
		this.within = 8;
	}

}
