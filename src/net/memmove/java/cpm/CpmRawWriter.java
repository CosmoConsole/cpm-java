package net.memmove.java.cpm;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;

import javax.imageio.ImageIO;

public class CpmRawWriter extends CpmWriter {

	@Override
	public void convertPNGtoCPM(File png, File cpm, boolean output) throws Exception {
		BufferedImage src = ImageIO.read(png);
		BitOutputStream b = new BitOutputStream();
		int w = src.getWidth();
		int h = src.getHeight();
		b.writeVariable(0x43504D1AL, 32);		// magic number
		b.writeVariable(0x0000, 16);			// v1
		b.writeVariable(0x08, 8);				// 8 bpcc
		b.writeVariable(0x00, 8);				// no palette
		b.writeVariable(w, 32);					// width
		b.writeVariable(h, 32);					// height
		b.writeVariable(0, 32);					// intended length #1 (post-written)
		b.writeVariable(0, 32);					// intended length #2 (post-written)
		b.writeVariable(0, 8);					// flags
		b.writeVariable(0, 24);					// unused #1
		b.writeVariable(0, 16);					// unused #2
		b.writeVariable(0xF, 8);				// include ARGB
		b.writeVariable(0, 8);					// not interlaced
		long seqc = 0;
		long remaining = 0;
		long pix = 0;
		for (int y = 0; y < h; y++)
			for (int x = 0; x < w; x++) {
				if (remaining == 0) {
					/*if (output)
						System.out.println("sequence at x=" + x + ",y=" + y + ",POS="+b.pos);*/
					seqc++;
					long z = Math.min(((long)h - (long)y - 1) * (long)w + (long)w - (long)x, 16384);
					b.writeVariable(0, 6);
					b.writeVariable(z - 1, 14);
					pix += z;
					remaining = z;
				}
				b.writeVariable(src.getRGB(x, y), 32);
				remaining--;
			}
		pix += remaining;
		b.writeVariable((0x1C<<14)|0x3FFF, 20);
		b.pad();
		b.writeVariable(0x39273339, 32);
		byte[] z = b.toByteArray();
		if (output)
			System.out.println("Writing " + seqc + " sequences, which account to " + pix + " pixels");
		z[0x10] = (byte)((seqc >> 56) & 0xFF);	// write sequence count
		z[0x11] = (byte)((seqc >> 48) & 0xFF);
		z[0x12] = (byte)((seqc >> 40) & 0xFF);
		z[0x13] = (byte)((seqc >> 32) & 0xFF);
		z[0x14] = (byte)((seqc >> 24) & 0xFF);
		z[0x15] = (byte)((seqc >> 16) & 0xFF);
		z[0x16] = (byte)((seqc >>  8) & 0xFF);
		z[0x17] = (byte)((seqc >>  0) & 0xFF);
	    FileOutputStream fos = new FileOutputStream(cpm);
	    fos.write(z);
	    b.close();
	    fos.close();
	}



	@Override
	public void convertPNGtoCPM(File png, File cpm) throws Exception {
		convertPNGtoCPM(png, cpm, false);
	}
}
