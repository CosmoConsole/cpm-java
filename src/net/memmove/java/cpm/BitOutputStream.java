package net.memmove.java.cpm;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class BitOutputStream extends OutputStream {
	ByteArrayOutputStream stream = new ByteArrayOutputStream();
	byte[] bits = new byte[128];
	int bp = 0;
	int pos = 0;
	@Override
	public void write(int arg0) throws IOException {
		this.writeVariable(arg0, 8);
	}
	public void writeVariable(long arg0, int bitsize) throws IOException {
		long data = arg0 & ((1L<<bitsize)-1);
		for (int i = bitsize - 1; i >= 0; i--)
			bits[bp++] = (byte)((data&(1<<i))>>i);
		int dp = 0;
		int sp = bp;
		while (sp >= 8) {
			byte b = 0;
			for (int i = 0; i < 8; i++)
				b |= (bits[i|(dp<<3)])<<(7-i);
			stream.write(b);
			pos++;
			dp++;
			sp -= 8;
		}
		if (dp > 0) {
			int bs = bits.length;
			System.arraycopy(bits, dp<<3, bits, 0, bs - (dp<<3));
			for (int i = bs - (dp<<3); i < bs; i++)
				bits[i] = 0;
			bp -= (dp<<3);
		}
	}
	@Override
	public void close() throws IOException {
		stream.close();
	}
	@Override
	public void flush() throws IOException {
		stream.flush();
	}
	public byte[] toByteArray() {
		return stream.toByteArray();
	}
	public void pad() throws IOException {
		if (bp < 1) return;
		this.writeVariable(0, 8-bp);
	}
	public void copyTo(BitOutputStream b) throws IOException {
		byte[] s = this.toByteArray();
		for (byte k: s)
			b.writeVariable(k, 8);
		int v = 0;
		for (int i = 0; i < bp; i++)
			v |= (bits[i])<<(bp-1-i);
		b.writeVariable(v, bp);
	}
	public void clear() throws IOException {
		bits = new byte[128];
		bp = 0;
		pos = 0;
		stream.close();
		stream = new ByteArrayOutputStream();
	}
}
