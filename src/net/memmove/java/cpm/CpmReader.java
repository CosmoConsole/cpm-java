package net.memmove.java.cpm;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import javax.imageio.ImageIO;

public class CpmReader {
	long[] palette;
	public void convertCPMtoPNG(File cpmFile, File pngFile, boolean output) throws Exception {
		BitInputStream b = new BitInputStream(Files.readAllBytes(cpmFile.toPath()));
		try {
			long header = b.readVariable(32);
			if (header < 0)
				throw new IOException("end-of-file not expected");
			if (header != 0x43504D1AL)
				throw new IOException("illegal format: magic number not present (0x" + String.format("%08x", header) + ")");
			long iformat = b.readVariable(16);
			if (iformat < 0)
				throw new IOException("end-of-file not expected");
			if (iformat != 0)
				throw new IOException("illegal format: do not recognize internal format (0x" + String.format("%08x", iformat) + ")");
			int N = (int) b.readVariable(8);
			if (N < 0)
				throw new IOException("end-of-file not expected");
			if (N == 0)
				throw new IOException("0bpp images are the future");	
			int P = (int) b.readVariable(8);
			if (P < 0)
				throw new IOException("end-of-file not expected");
			long W = b.readVariable(32);
			long H = b.readVariable(32);
			if (W < 0 || H < 0) 
				throw new IOException("end-of-file not expected");
			BufferedImage image = new BufferedImage((int)W, (int)H, BufferedImage.TYPE_INT_ARGB);
			long seqc = b.readVariable(64);
			if (seqc < 0)
				throw new IOException("end-of-file not expected");
			long seqf = 0;
			int flags = (int)b.readVariable(8);
			if (flags < 0)
				throw new IOException("end-of-file not expected");
			b.readVariable(40);
			int channels = (int)b.readVariable(8);
			if (channels < 0)
				throw new IOException("end-of-file not expected");
			int interlace = (int)b.readVariable(8);
			if (interlace < 0)
				throw new IOException("end-of-file not expected");
			if (interlace != 0)
				throw new IOException("illegal format: do not recognize interlace format (0x" + String.format("%08x", interlace) + ")");
			// no palette support... yet
			int A = 255, R = 0, G = 0, B = 0;
			if (P > 0) {
				palette = new long[1<<P];
				for (int i = 0; i < (1<<P); i++) {
					if ((channels&0x08)!=0) 
						A = (int) ((b.readVariable(N)*255)/((1<<N)-1));
					if ((channels&0x04)!=0) 
						R = (int) ((b.readVariable(N)*255)/((1<<N)-1));
					if ((channels&0x02)!=0) 
						G = (int) ((b.readVariable(N)*255)/((1<<N)-1));
					if ((channels&0x01)!=0) 
						B = (int) ((b.readVariable(N)*255)/((1<<N)-1));
					palette[i] = (A<<24)|(R<<16)|(G<<8)|B;
				}
			}
			// start reading commands
			long x = 0;
			long y = 0;
			long BG = 0x00000000;
			A = 255;
			R = 0;
			G = 0;
			B = 0;
			while (true) {
				long c = b.readVariable(6);
				if (c < 0)
					throw new IOException("no terminator found");
				long l = b.readVariable(14);
				if (l < 0)
					throw new IOException("no terminator found");
				if (c == 0x1C && l == 0x3FFF) {
					b.pad();
					break;
				}
				seqf++;
				if (c != 0xA) {
					/*if (output && c < 4) {
						System.out.println("sequence at x=" + x + ",y=" + y + ": " + c + "," + l);
						System.out.print("POS=" + b.pos + " | ");
					}*/
					for (long i = 0; i <= l; i++) {
						boolean draw = false;
						if (c >= 0 && c < 4) {							// ABSOLUTE_COLOR_DIV_N
							int V = N >> c;
							if ((channels&0x08)!=0) 
								A = (int) ((b.readVariable(V)*255)/((1<<V)-1));
							if ((channels&0x04)!=0) 
								R = (int) ((b.readVariable(V)*255)/((1<<V)-1));
							if ((channels&0x02)!=0) 
								G = (int) ((b.readVariable(V)*255)/((1<<V)-1));
							if ((channels&0x01)!=0) 
								B = (int) ((b.readVariable(V)*255)/((1<<V)-1));
							draw = true;
						}
						else if (c >= 4 && c < 8) {							// PALETTE_DIV_N
							long V = palette[(int) b.readVariable(P)];
							A = (int) ((V&0xFF000000)>>24);
							R = (int) ((V&0x00FF0000)>>16);
							G = (int) ((V&0x0000FF00)>> 8);
							B = (int) ((V&0x000000FF)    );
							draw = true;
						}
						else if (c == 8) {									// SET_FIRST_PALETTE
							if (l >= (1<<P))
								throw new IOException("too long palette change sequence");
							if ((channels&0x08)!=0) 
								A = (int) ((b.readVariable(N)*255)/((1<<N)-1));
							if ((channels&0x04)!=0) 
								R = (int) ((b.readVariable(N)*255)/((1<<N)-1));
							if ((channels&0x02)!=0) 
								G = (int) ((b.readVariable(N)*255)/((1<<N)-1));
							if ((channels&0x01)!=0) 
								B = (int) ((b.readVariable(N)*255)/((1<<N)-1));
							palette[(int) i] = (A<<24)|(R<<16)|(G<<8)|B;
						}
						else if (c == 9) {									// METADATA, IGNORE
						}
						else if (c == 0xB) {								// BACKGROUND_PIXEL
							A = (int) ((BG&0xFF000000)>>24);
							R = (int) ((BG&0x00FF0000)>>16);
							G = (int) ((BG&0x0000FF00)>> 8);
							B = (int) ((BG&0x000000FF)    );
							draw = true;
						}
						else if (c == 0xC) {								// SAME_PIXEL_LEFT
							if (x == 0)
								throw new IOException("SAME_PIXEL_LEFT when x=0");
							long V = image.getRGB((int)(x-1), (int)(y));
							A = (int) ((V&0xFF000000)>>24);
							R = (int) ((V&0x00FF0000)>>16);
							G = (int) ((V&0x0000FF00)>> 8);
							B = (int) ((V&0x000000FF)    );
							draw = true;
						}
						else if (c == 0xD) {								// SAME_PIXEL_TOP
							if (y == 0)
								throw new IOException("SAME_PIXEL_TOP when y=0");
							long V = image.getRGB((int)(x), (int)(y-1));
							A = (int) ((V&0xFF000000)>>24);
							R = (int) ((V&0x00FF0000)>>16);
							G = (int) ((V&0x0000FF00)>> 8);
							B = (int) ((V&0x000000FF)    );
							draw = true;
						}
						else if (c == 0xE) {								// SAME_PIXEL_TOPLEFT
							if (x == 0 || y == 0)
								throw new IOException("SAME_PIXEL_TOPLEFT when x=0 or y=0");
							long V = image.getRGB((int)(x-1), (int)(y-1));
							A = (int) ((V&0xFF000000)>>24);
							R = (int) ((V&0x00FF0000)>>16);
							G = (int) ((V&0x0000FF00)>> 8);
							B = (int) ((V&0x000000FF)    );
							draw = true;
						}
						else if (c == 0xF) {								// SAME_PIXEL_LASTREAD
							if (x == 0 && y == 0)
								throw new IOException("SAME_PIXEL_LASTREAD and first pixel");
							draw = true;
						}
						else if (c >= 0x10 && c < 0x14) {					// RELATIVE_LEFT_COLOR_DIV_N
							if (x == 0)
								throw new IOException("RELATIVE_LEFT_COLOR when x=0");
							long Q = image.getRGB((int)(x-1), (int)(y));
							A = (int) ((Q&0xFF000000)>>24);
							R = (int) ((Q&0x00FF0000)>>16);
							G = (int) ((Q&0x0000FF00)>> 8);
							B = (int) ((Q&0x000000FF)    );
							int V = op(N, c&3);
							if ((channels&0x08)!=0) 
								A = ((int) (A + fixSign(b.readVariable(V),V)*255))&0xFF;
							if ((channels&0x04)!=0) 
								R = ((int) (R + fixSign(b.readVariable(V),V)*255))&0xFF;
							if ((channels&0x02)!=0) 
								G = ((int) (G + fixSign(b.readVariable(V),V)*255))&0xFF;
							if ((channels&0x01)!=0) 
								B = ((int) (B + fixSign(b.readVariable(V),V)*255))&0xFF;
							draw = true;
						}
						else if (c >= 0x14 && c < 0x18) {					// RELATIVE_TOP_COLOR_DIV_N
							if (y == 0)
								throw new IOException("RELATIVE_TOP_COLOR when x=0");
							long Q = image.getRGB((int)(x), (int)(y-1));
							A = (int) ((Q&0xFF000000)>>24);
							R = (int) ((Q&0x00FF0000)>>16);
							G = (int) ((Q&0x0000FF00)>> 8);
							B = (int) ((Q&0x000000FF)    );
							int V = op(N, c&3);
							if ((channels&0x08)!=0) 
								A = ((int) (A + fixSign(b.readVariable(V),V)*255))&0xFF;
							if ((channels&0x04)!=0) 
								R = ((int) (R + fixSign(b.readVariable(V),V)*255))&0xFF;
							if ((channels&0x02)!=0) 
								G = ((int) (G + fixSign(b.readVariable(V),V)*255))&0xFF;
							if ((channels&0x01)!=0) 
								B = ((int) (B + fixSign(b.readVariable(V),V)*255))&0xFF;
							draw = true;
						}
						else if (c >= 0x18 && c < 0x1C) {					// RELATIVE_TOPLEFT_COLOR_DIV_N
							if (x == 0 || y == 0)
								throw new IOException("RELATIVE_TOPLEFT_COLOR when x=0 or y=0");
							long Q = image.getRGB((int)(x-1), (int)(y-1));
							A = (int) ((Q&0xFF000000)>>24);
							R = (int) ((Q&0x00FF0000)>>16);
							G = (int) ((Q&0x0000FF00)>> 8);
							B = (int) ((Q&0x000000FF)    );
							int V = op(N, c&3);
							if ((channels&0x08)!=0) 
								A = ((int) (A + fixSign(b.readVariable(V),V)*255))&0xFF;
							if ((channels&0x04)!=0) 
								R = ((int) (R + fixSign(b.readVariable(V),V)*255))&0xFF;
							if ((channels&0x02)!=0) 
								G = ((int) (G + fixSign(b.readVariable(V),V)*255))&0xFF;
							if ((channels&0x01)!=0) 
								B = ((int) (B + fixSign(b.readVariable(V),V)*255))&0xFF;
							draw = true;
						}
						else if (c >= 0x1C && c < 0x20) {					// RELATIVE_LASTREAD_COLOR_DIV_N
							if (x == 0 && y == 0)
								throw new IOException("RELATIVE_LASTREAD_COLOR and first pixel");
							int V = op(N, c&3);
							if ((channels&0x08)!=0) 
								A = ((int) (A + fixSign(b.readVariable(V),V)*255))&0xFF;
							if ((channels&0x04)!=0) 
								R = ((int) (R + fixSign(b.readVariable(V),V)*255))&0xFF;
							if ((channels&0x02)!=0) 
								G = ((int) (G + fixSign(b.readVariable(V),V)*255))&0xFF;
							if ((channels&0x01)!=0) 
								B = ((int) (B + fixSign(b.readVariable(V),V)*255))&0xFF;
							draw = true;
						}
						if (draw) {
							if (y >= H) {
								throw new IOException("too many pixels on surface!");
							} else {
								image.setRGB((int)x, (int)y, (A<<24)|(R<<16)|(G<<8)|(B));
								x++;
								if (x == W) {
									x = 0;
									y++;
								}
							}
						}
					}
					/*if (output)
						System.out.println(b.pos);*/
				} else {
					if (l == 0) {
						if ((channels&0x08)!=0) 
							A = (int) ((b.readVariable(N)*255)/((1<<N)-1));
						if ((channels&0x04)!=0) 
							R = (int) ((b.readVariable(N)*255)/((1<<N)-1));
						if ((channels&0x02)!=0) 
							G = (int) ((b.readVariable(N)*255)/((1<<N)-1));
						if ((channels&0x01)!=0) 
							B = (int) ((b.readVariable(N)*255)/((1<<N)-1));
						BG = (A<<24)|(R<<16)|(G<<8)|B;
					} else if (l == 1) {
						b.readVariable(N);
						b.readVariable(N);
						b.readVariable(N);
						b.readVariable(N);
						A = (int) ((b.readVariable(N)*255)/((1<<N)-1));
						R = (int) ((b.readVariable(N)*255)/((1<<N)-1));
						G = (int) ((b.readVariable(N)*255)/((1<<N)-1));
						B = (int) ((b.readVariable(N)*255)/((1<<N)-1));
					}
				}
			}
			if (seqf > seqc)
				throw new IOException("too many sequences found in comparison to sequence count (found " + seqf + ", expected " + seqc + ")");
			if (seqf < seqc)
				throw new IOException("not enough sequences found in comparison to sequence count (found " + seqf + ", expected " + seqc + ")");
			if (x != 0 && y != H)
				throw new IOException("illegal amount of pixels (should be " + (W * H) + ", was " + ((y * H) + x) + ")");
			b.pad();
			long footer = b.readVariable(32);
			if (footer != 0x39273339)
				throw new IOException("footer too short (terminator cut off)");
			ImageIO.write(image, "png", pngFile);
		} catch (Exception ex) {
			b.close();
			throw ex;
		} finally {
			b.close();
		}
	}
	private int op(int n, long c) {
		if (c == 0) return (n*3)>>2;
		if (c == 1) return n>>1;
		if (c == 2) return (n*3)>>3;
		if (c == 3) return n>>2;
		return 0;
	}
	private long fixSign(long v, int b) {
		if (v >= (1<<(b-1))) {
			return v-(1<<b);
		}
		return v;
	}

}
