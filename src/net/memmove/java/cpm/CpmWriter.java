package net.memmove.java.cpm;

import java.io.File;

public abstract class CpmWriter {
	public abstract void convertPNGtoCPM(File png, File cpm) throws Exception;
	public abstract void convertPNGtoCPM(File png, File cpm, boolean output) throws Exception;
}
