package net.memmove.java.cpm;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;

import javax.imageio.ImageIO;

public class CpmO1Writer extends CpmWriter {

	@Override
	public void convertPNGtoCPM(File png, File cpm, boolean output) throws Exception {
		BufferedImage src = ImageIO.read(png);
		BitOutputStream b = new BitOutputStream();
		int w = src.getWidth();
		int h = src.getHeight();
		b.writeVariable(0x43504D1AL, 32);		// magic number
		b.writeVariable(0x0000, 16);			// v1
		b.writeVariable(0x08, 8);				// 8 bpcc
		b.writeVariable(0x000, 8);				// no palette
		b.writeVariable(w, 32);					// width
		b.writeVariable(h, 32);					// height
		b.writeVariable(0, 32);					// intended length #1 (post-written)
		b.writeVariable(0, 32);					// intended length #2 (post-written)
		b.writeVariable(0, 8);					// flags
		b.writeVariable(0, 24);					// unused #1
		b.writeVariable(0, 16);					// unused #2
		b.writeVariable(0xF, 8);				// include ARGB
		b.writeVariable(0, 8);					// not interlaced
		long seqc = 0;
		long pix = 0;
		
		// READ GRID
		
		int[][][] grid = new int[h][w][5];
		int[][] tape = new int[h*w ][5];
		for (int y = 0; y < h; y++)
			for (int x = 0; x < w; x++) {
				long c = src.getRGB(x, y);
				grid[y][x][0] = 0;
				grid[y][x][1] = (int)((c>>24)&0xFF);
				grid[y][x][2] = (int)((c>>16)&0xFF);
				grid[y][x][3] = (int)((c>> 8)&0xFF);
				grid[y][x][4] = (int)((c    )&0xFF);
			}
		
		// OPTIMIZE GRID
		for (int y = 0; y < h; y++)
			for (int x = 0; x < w; x++) {
				if (grid[y][x][1]==0) {
					grid[y][x][0]=0xB;
				}
			}
		
		// *0.750 (6-bit) ranges: -32 to +32
		// *0.500 (4-bit) ranges: - 8 to + 8
		// *0.375 (3-bit) ranges: - 4 to + 4
		// *0.250 (2-bit) ranges: - 2 to + 2
		
		for (int y = 0; y < h; y++)
			for (int x = w - 1; x >= 0; x--) {
				if (x > 0) {
					if (grid[y][x][0] != 0) continue;
					int dA = - grid[y][x][1] + grid[y][x-1][1];
					int dR = - grid[y][x][2] + grid[y][x-1][2];
					int dG = - grid[y][x][3] + grid[y][x-1][3];
					int dB = - grid[y][x][4] + grid[y][x-1][4];
					int maxD = mostaway(dA, mostaway(dR, mostaway(dG, dB)));
					int maxS = signedBitsNeeded(maxD);
					if (maxS == 0) {
						// SAME
						grid[y][x][0] = 0x0C;
						grid[y][x][1] = 0;
						grid[y][x][2] = 0;
						grid[y][x][3] = 0;
						grid[y][x][4] = 0;
					} else if (maxS <= 2) {
						// 0.250
						grid[y][x][0] = 0x13;
						grid[y][x][1] = sign(dA,2);
						grid[y][x][2] = sign(dR,2);
						grid[y][x][3] = sign(dG,2);
						grid[y][x][4] = sign(dB,2);
					} else if (maxS <= 3) {
						// 0.375
						grid[y][x][0] = 0x12;
						grid[y][x][1] = sign(dA,3);
						grid[y][x][2] = sign(dR,3);
						grid[y][x][3] = sign(dG,3);
						grid[y][x][4] = sign(dB,3);
					} else if (maxS <= 4) {
						// 0.500
						grid[y][x][0] = 0x11;
						grid[y][x][1] = sign(dA,4);
						grid[y][x][2] = sign(dR,4);
						grid[y][x][3] = sign(dG,4);
						grid[y][x][4] = sign(dB,4);
					} else if (maxS <= 6) {
						// 0.750
						grid[y][x][0] = 0x10;
						grid[y][x][1] = sign(dA,6);
						grid[y][x][2] = sign(dR,6);
						grid[y][x][3] = sign(dG,6);
						grid[y][x][4] = sign(dB,6);
					}
				}
			}

		// GRID -> TAPE
		long tapelen = ((long)(w)*(long)(h));
		long tp = 0;
		for (int y = 0; y < h; y++)
			for (int x = 0; x < w; x++) {
				for (int z = 0; z < grid[y][x].length; z++) {
					tape[(int)tp][z] = grid[y][x][z];
				}
				tp++;
			}
		grid = null;
		
		// OPTIMIZE TAPE
		int lastSeq = -1;
		long lastLen = 0;
		long lastStart = 0;
		boolean found = true;
		while (found) {
			found = false;
			for (long t = 0; t < tapelen; t++) {
				// relative stride compression
				if (lastSeq < 0) {
					lastSeq = tape[(int)t][0];
					lastStart = t;
				} else if (lastSeq != tape[(int)t][0]) {
					// get left and right
					if (lastStart > 0) {
						int leftSeq = tape[(int)(lastStart-1)][0];
						int rightSeq = tape[(int)t][0];
						int seqHeaderSize = 20;
						int[] bitsPerSeq = new int[]{-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, -1, -1, -1, -1, 6, 4, 3, 2, 6, 4, 3, 2, 6, 4, 3, 2, 6, 4, 3, 2, -1, -1, -1, -1, 6, 4, 3, 2, 6, 4, 3, 2, 6, 4, 3, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0};
						if (lastLen < 20) {
							long currentCost = seqHeaderSize + bitsPerSeq[lastSeq] * lastLen;
							if (bitsPerSeq[lastSeq] >= 0) {
								if (leftSeq != rightSeq) {
									if ((bitsPerSeq[leftSeq] > bitsPerSeq[lastSeq]) && (bitsPerSeq[rightSeq] > bitsPerSeq[lastSeq])) {
										// both are acceptable, calculate one with most benefit
										long leftCost = sign(bitsPerSeq[leftSeq],22) * lastLen;
										long rightCost = sign(bitsPerSeq[rightSeq],22) * lastLen;
										if ((bitsPerSeq[leftSeq] > bitsPerSeq[rightSeq]) && (currentCost > leftCost)) {
											for (int i = 0; i < lastLen; i++) {
												tape[(int)(lastStart+i)][0] = leftSeq;
												for (int j = 1; j <= 4; j++) {
													tape[(int)(lastStart+i)][j] = signExtend(tape[(int)(lastStart+i)][j], bitsPerSeq[lastSeq], bitsPerSeq[leftSeq]);
												}
											}
										} else if ((bitsPerSeq[rightSeq] > bitsPerSeq[leftSeq]) && (currentCost > rightCost)) {
											for (int i = 0; i < lastLen; i++) {
												tape[(int)(lastStart+i)][0] = rightSeq;
												for (int j = 1; j <= 4; j++) {
													tape[(int)(lastStart+i)][j] = signExtend(tape[(int)(lastStart+i)][j], bitsPerSeq[lastSeq], bitsPerSeq[rightSeq]);
												}
											}
										}
									}
								} else {
									if ((bitsPerSeq[leftSeq] > bitsPerSeq[lastSeq]) && (bitsPerSeq[leftSeq] >= 0)) {
										long leftCost = bitsPerSeq[leftSeq] * lastLen;
										if (currentCost > leftCost) {
											for (int i = 0; i < lastLen; i++) {
												tape[(int)(lastStart+i)][0] = leftSeq;
												for (int j = 1; j <= 4; j++) {
													tape[(int)(lastStart+i)][j] = signExtend(tape[(int)(lastStart+i)][j], bitsPerSeq[lastSeq], bitsPerSeq[leftSeq]);
												}
											}
										}
									}
								}
							}
						}
					}
					lastSeq = tape[(int)t][0];
					lastLen = 0;
					lastStart = t;
				}
				lastLen++;
			}
		}
		
		// WRITE TAPE
		int lastcode = -1;
		int curlen = 0;
		BitOutputStream backup = null;
		for (long t = 0; t < tapelen; t++) {
			int c = tape[(int)t][0];
			if (lastcode < 0) {
				b.writeVariable(c, 6);
				seqc++;
				curlen = 0;
				lastcode = c;
				backup = new BitOutputStream();
			} else if (c != lastcode || curlen >= 16384) {
				b.writeVariable(curlen-1, 14);
				pix += curlen;
				seqc++;
				curlen = 0;
				lastcode = c;
				backup.copyTo(b);
				backup.clear();
				b.writeVariable(c, 6);
			}
			curlen++;
			if (c == 0x0) {
				long color = (tape[(int)t][1]<<24)|(tape[(int)t][2]<<16)|(tape[(int)t][3]<<8)|(tape[(int)t][4]);
				backup.writeVariable(color, 32);
			} else if (c == 0x10) {
				int W = 6;
				for (int i = 1; i <= 4; i++)
					backup.writeVariable(tape[(int)t][i], W);
			} else if (c == 0x11) {
				int W = 4;
				for (int i = 1; i <= 4; i++)
					backup.writeVariable(tape[(int)t][i], W);
			} else if (c == 0x12) {
				int W = 3;
				for (int i = 1; i <= 4; i++)
					backup.writeVariable(tape[(int)t][i], W);
			} else if (c == 0x13) {
				int W = 2;
				for (int i = 1; i <= 4; i++)
					backup.writeVariable(tape[(int)t][i], W);
			}
		}
		if (backup != null) {
			b.writeVariable(curlen-1, 14);
			pix += curlen;
			backup.copyTo(b);
			backup.close();
		}	
		b.writeVariable((0x1C<<14)|0x3FFF, 20);
		b.pad();
		b.writeVariable(0x39273339, 32);
		byte[] z = b.toByteArray();
		if (output)
			System.out.println("Writing " + seqc + " sequences, which account to " + pix + " pixels (compare to " + (((long)w)*((long)h)) + ")");
		z[0x10] = (byte)((seqc >> 56) & 0xFF);	// write sequence count
		z[0x11] = (byte)((seqc >> 48) & 0xFF);
		z[0x12] = (byte)((seqc >> 40) & 0xFF);
		z[0x13] = (byte)((seqc >> 32) & 0xFF);
		z[0x14] = (byte)((seqc >> 24) & 0xFF);
		z[0x15] = (byte)((seqc >> 16) & 0xFF);
		z[0x16] = (byte)((seqc >>  8) & 0xFF);
		z[0x17] = (byte)((seqc >>  0) & 0xFF);
	    FileOutputStream fos = new FileOutputStream(cpm);
	    fos.write(z);
	    b.close();
	    fos.close();
	}



	private int mostaway(int a, int b) {
		if (signedBitsNeeded(a) > signedBitsNeeded(b))
			return a;
		return b;
	}



	private int signedBitsNeeded(int a) {
		if (a == 0) return 0;
		for (int e = 1; e < 50; e++) {
			if ((a >= -(1<<e)) && (a < (1<<e)))
				return e + 1;
		}
		return 50;
	}



	private int sign(int d, int v) {
		if (d < 0) {
			return d + (1<<v);
		}
		return d;
	}
	
	private int signExtend(int d, int v1, int v2) {
		if (d == 0) return 0;
		if (d >= (1<<(v1-1))) {
			return (d-(1<<v1))+(1<<v2);
		}
		return d;
	}



	@Override
	public void convertPNGtoCPM(File png, File cpm) throws Exception {
		convertPNGtoCPM(png, cpm, false);
	}
}
