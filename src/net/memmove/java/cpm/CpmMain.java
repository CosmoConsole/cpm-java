package net.memmove.java.cpm;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

import org.tukaani.xz.LZMA2Options;
import org.tukaani.xz.XZInputStream;
import org.tukaani.xz.XZOutputStream;

public class CpmMain {

	public static final String VERSION = "0.1a";
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length < 3) {
			printHelp();
			return;
		}
		if (args[0].equalsIgnoreCase("encode")) {
			File pngFile, cpmFile;
			try {
				pngFile = new File(args[1]);
				cpmFile = new File(args[2]);
			} catch (Exception ex) {
				System.out.println("Incorrect file format");
				return;
			}
			boolean raw = false;
			for (int i = 3; i < args.length; i++) {
				if (args[i].toLowerCase() == "-r") {
					raw = true;
				} else if (args[i].toLowerCase() == "--") break;
			}
			if (!pngFile.isFile()) {
				System.out.println("Source file does not exist");
				return;
			}
			if (cpmFile.isDirectory()) {
				System.out.println("Cannot overwrite directory");
				return;
			}
			if (cpmFile.isFile()) {
				String c = "";
				Scanner s = new Scanner(System.in);
				while (!(c.equalsIgnoreCase("y") || c.equalsIgnoreCase("n"))) {
					System.out.print("Destination file already exists. Overwrite (Y/N)?");
					c = s.nextLine();
				}
				s.close();
				if (c.equalsIgnoreCase("n"))
					return;
			}
			CpmWriter cw;
			if (raw)
				cw = new CpmRawWriter();
			else
				cw = new CpmO1Writer();
			try {
				cw.convertPNGtoCPM(pngFile, cpmFile, true);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (args[0].equalsIgnoreCase("pack")) {
			File pngFile, cpmFile;
			try {
				pngFile = new File(args[1]);
				cpmFile = new File(args[2]);
			} catch (Exception ex) {
				System.out.println("Incorrect file format");
				return;
			}
			boolean raw = false;
			for (int i = 3; i < args.length; i++) {
				if (args[i].toLowerCase() == "-r") {
					raw = true;
				} else if (args[i].toLowerCase() == "--") break;
			}
			if (!pngFile.isFile()) {
				System.out.println("Source file does not exist");
				return;
			}
			if (cpmFile.isDirectory()) {
				System.out.println("Cannot overwrite directory");
				return;
			}
			if (cpmFile.isFile()) {
				String c = "";
				Scanner s = new Scanner(System.in);
				while (!(c.equalsIgnoreCase("y") || c.equalsIgnoreCase("n"))) {
					System.out.print("Destination file already exists. Overwrite (Y/N)?");
					c = s.nextLine();
				}
				s.close();
				if (c.equalsIgnoreCase("n"))
					return;
			}
			CpmWriter cw;
			if (raw)
				cw = new CpmRawWriter();
			else
				cw = new CpmO1Writer();
			try {
				cw.convertPNGtoCPM(pngFile, cpmFile, true);
				File cpmTempFile = new File(cpmFile.getAbsolutePath() + "." + (new Random().nextLong())); 
				while (cpmTempFile.exists())
					cpmTempFile = new File(cpmFile.getAbsolutePath() + "." + (new Random().nextLong())); 
				cpmFile.renameTo(cpmTempFile);
				LZMA2Options options = new LZMA2Options();
				FileInputStream inp = new FileInputStream(cpmTempFile);
				FileOutputStream fout = new FileOutputStream(cpmFile);
				fout.write(new byte[]{(byte) 0xFF, 0x43, 0x50, 0x4D, 0x20, 0x58, 0x5A, 0x0D, 0x0A, 0x1A, 0x0A, 0x00, 0x00, 0x00, 0x00, 0x00});
		        XZOutputStream out = new XZOutputStream(fout, options);
		        byte[] buf = new byte[8192];
		        int size;
		        while ((size = inp.read(buf)) != -1)
		            out.write(buf, 0, size);
		        out.finish();
		        out.close();
		        inp.close();
		        if (!cpmTempFile.delete())
		        	cpmTempFile.deleteOnExit();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (args[0].equalsIgnoreCase("decode")) {
			File pngFile, cpmFile;
			try {
				cpmFile = new File(args[1]);
				pngFile = new File(args[2]);
			} catch (Exception ex) {
				System.out.println("Incorrect file format");
				return;
			}
			if (!cpmFile.isFile()) {
				System.out.println("Source file does not exist");
				return;
			}
			if (pngFile.isDirectory()) {
				System.out.println("Cannot overwrite directory");
				return;
			}
			if (pngFile.isFile()) {
				String c = "";
				Scanner s = new Scanner(System.in);
				while (!(c.equalsIgnoreCase("y") || c.equalsIgnoreCase("n"))) {
					System.out.print("Destination file already exists. Overwrite (Y/N)?");
					c = s.nextLine();
				}
				s.close();
				if (c.equalsIgnoreCase("n"))
					return;
			}
			CpmReader cw = new CpmReader();
			try {
				cw.convertCPMtoPNG(cpmFile, pngFile, true);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (args[0].equalsIgnoreCase("unpack")) {
			File pngFile, cpmFile;
			try {
				cpmFile = new File(args[1]);
				pngFile = new File(args[2]);
			} catch (Exception ex) {
				System.out.println("Incorrect file format");
				return;
			}
			if (!cpmFile.isFile()) {
				System.out.println("Source file does not exist");
				return;
			}
			if (pngFile.isDirectory()) {
				System.out.println("Cannot overwrite directory");
				return;
			}
			if (pngFile.isFile()) {
				String c = "";
				Scanner s = new Scanner(System.in);
				while (!(c.equalsIgnoreCase("y") || c.equalsIgnoreCase("n"))) {
					System.out.print("Destination file already exists. Overwrite (Y/N)?");
					c = s.nextLine();
				}
				s.close();
				if (c.equalsIgnoreCase("n"))
					return;
			}
			File cpmTempFile = new File(cpmFile.getAbsolutePath() + "." + (new Random().nextLong())); 
			while (cpmTempFile.exists())
				cpmTempFile = new File(cpmFile.getAbsolutePath() + "." + (new Random().nextLong())); 
			FileInputStream in = null;
			FileOutputStream out = null;
			try {
				in = new FileInputStream(cpmFile);
				out = new FileOutputStream(cpmTempFile);
				byte[] buf = new byte[8192];
				int size = 0;
				byte[] intro = new byte[16];
				if (in.read(intro) < 16) {
					throw new IOException("EOF not expected");
				}
				byte[] header = new byte[]{(byte) 0xFF, 0x43, 0x50, 0x4D, 0x20, 0x58, 0x5A, 0x0D, 0x0A, 0x1A, 0x0A, 0x00};
				for (int j = 0; j<12; j++)
					if (header[j] != intro[j]) {
						throw new IOException("illegal CPM header");
					}
				long ver = (intro[12]<<24)|(intro[13]<<16)|(intro[14]<<8)|(intro[15]);
				if (ver != 0)
					throw new IOException("cannot recognize internal CPM-XZ version (0x" + String.format("%08x", ver) + ")");
				XZInputStream xin = new XZInputStream(in);
				while ((size = xin.read(buf)) >= 0)
					out.write(buf, 0, size);
				xin.close();
			} catch (IOException e1) {
				e1.printStackTrace();
				return;
			} finally {
				try {
					if (in != null) in.close();
					if (out != null) out.close();
				} catch (IOException e2) {
					e2.printStackTrace();
					return;
				}
			}
			CpmReader cw = new CpmReader();
			try {
				cw.convertCPMtoPNG(cpmTempFile, pngFile, true);
			} catch (Exception e) {
				e.printStackTrace();
			}
	        cpmTempFile.deleteOnExit();
		} else {
			printHelp();
			return;
		}
	}
	/*private static void copyFile(File from, File to) throws IOException {
		final int BLOCK_SIZE = 16384;
		FileInputStream inp = new FileInputStream(from);
		FileOutputStream out = new FileOutputStream(to);
		byte[] buffer = new byte[BLOCK_SIZE];
		int size = 0;
		while ((size = inp.read(buffer)) >= 0)
			out.write(buffer, 0, size);
		out.close();
		inp.close();
	}*/
	private static void printHelp() {
		System.out.println("Usage: java -jar cpm-" + VERSION + ".jar (and then one of the following:)");
		System.out.println("        encode  pngimage.png    cpmimage.cpm       (FLAGS)");
		System.out.println("          pack  pngimage.png    cpmimage.cpm.xz    (FLAGS)");
		System.out.println("        decode  cpmimage.cpm    pngimage.png       (FLAGS)");
		System.out.println("        unpack  cpmimage.cpm.xz pngimage.png       (FLAGS)");
		System.out.println("");
		System.out.println("(FLAGS)");
		System.out.println("    encode / pack:");
		System.out.println("        -r : Raw encode (just pixel after pixel, do not optimize in any way)");
		System.out.println("        -i1 : Interlace with Adam7 when encoding");
		System.out.println("        -i2 : Interlace with GridPattern when encoding");
	}

}
